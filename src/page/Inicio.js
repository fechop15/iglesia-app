import React, {Component} from 'react';
import {
    Container,
    Header,
    Title,
    Right,
    Body,
    Icon,
    Text,
    Spinner,
    Footer,
    FooterTab,
    Button,
    Card,
    CardItem,
    Content,
    Thumbnail, Fab, ListItem, Left
} from 'native-base';

import {
    StyleSheet,
    FlatList,
    View,
    Platform,
    TouchableOpacity,
    Image,
    AsyncStorage,
    StatusBar, ImageBackground
} from 'react-native'

import CuentaPage from '../page/Cuenta';
import InvitadosPage from '../page/Invitados';
import MisInvitados from '../page/MisInvitados';
import NoticiasPage from '../page/Noticias';
import MethodsNoticias from "../methods/MethodsNoticias";

let menuAdmin = [{
    nombre: 'Miembros',
    icono: 'user',
    ruta: 'Usuarios'
},
];

let menuOtros = [{
    nombre: 'Miembros',
    icono: 'md-bookmarks',
    ruta: 'Usuarios'
},];

export default class Inicio extends Component {

    constructor(props) {
        super(props);
        this.Noticias = new MethodsNoticias();
        this.state = {

            noticias: [],
            usuario:
                {
                    id: "0",
                    nombre: "",
                    correo: "",
                    fecha: "",
                    rol: "",
                    telefono: "",
                    fecha_reg: "",
                    direccion: "",
                    api_token: "",
                    cargo: "",
                },
            cargaNoticias: true,
            footerTab: 1,
            titulo: "Inicio",
            btnAdd: false,
            active: false,
        }
    }

    renderTab = (number) => {
        switch (number) {
            case 1:
                return (
                    <View>
                        <Header>
                            <Body>
                                <Title style={styles.title}>Bienaventuranzas</Title>
                            </Body>
                            <Right/>
                        </Header>


                        <View style={styles.logoContainer}>
                            <Thumbnail
                                source={{uri: "https://gospelapp.lasbienaventuranzas.org/images/img-app/logo.png"}}
                                style={{height: 100, width: null, flex: 1}}
                            />
                        </View>

                        <Content style={styles.lastPredicaContainer}>

                            <Text style={styles.lastPredicaTitle}> Ultima Predica</Text>

                            {this.cargarNoticias()}
                        </Content>

                        <View style={styles.MainContainer}>
                            {this.cargarLista()}

                        </View>
                    </View>
                );
            case 2:

                //this.setState({btnAdd: true});

                return (
                    <NoticiasPage navigation={this.props.navigation}></NoticiasPage>
                );
            case 3:

                //this.setState({btnAdd: false});
                if (this.state.usuario.cargo === 'Pastor') {
                    return (
                        <InvitadosPage navigation={this.props.navigation}></InvitadosPage>
                    )
                } else {
                    return (
                        <MisInvitados navigation={this.props.navigation}></MisInvitados>
                    )
                }
            case 4:

                //this.setState({btnAdd: false});

                return (
                    <CuentaPage navigation={this.props.navigation}></CuentaPage>
                )
                break;
            default:
                return (
                    <View>
                        <Header>
                            <Body>
                                <Title style={styles.title}>Bienaventuranzas</Title>
                            </Body>
                            <Right/>
                        </Header>


                        <View style={styles.logoContainer}>
                            <Image source={logo} style={styles.logo}/>
                        </View>

                        <View style={styles.MainContainer}>
                            {this.cargarLista()}

                        </View>
                    </View>
                )
        }
    }

    changeTab = (number) => {
        if (this.state.footerTab !== number) {
            this.setState({footerTab: number});
            this.setState({btnAdd: (number === 3)});
        }
    }

    async componentDidMount() {
        await AsyncStorage.getItem('user')
            .then((value) => {
                    if (value != null) {
                        this.setState({usuario: JSON.parse(value)});
                    }
                }
            );

        this._isMounted = true;
        this._isMounted ? this.getLastPredica() : null;
        this.props.navigation.addListener('didFocus', () => {
            this._isMounted ? this.setState({cargaNoticias: true}) : null;
            this._isMounted ? this.getLastPredica() : null;
        });

        //this.setState({usuario: (this.props.navigation.getParam('usuario'))});
        //alert(JSON.stringify(this.state.usuario));

    }

    cargarLista() {
        //alert(JSON.stringify(this.state.usuario));

        if (this.state.usuario.rol === "") {
            return (<Spinner color='blue'/>);
        }

        return (

            <View>
                <Text style={styles.lastPredicaTitle}> Directorio</Text>

                <FlatList

                    data={this.state.usuario.rol === "Super Administr" ? menuAdmin : menuOtros}

                    renderItem={({item}) =>
                        <View style={styles.container}>
                            <TouchableOpacity style={styles.GridViewBlockStyle}
                                              onPress={this.GetGridViewItem.bind(this, item.ruta)}>
                                <Icon
                                    name={item.icono}
                                    style={styles.iconBtns}
                                />
                            </TouchableOpacity>
                        </View>


                    }

                    keyExtractor={(item, index) => index.toString()}
                    numColumns={3}
                />
            </View>)
    }

    cargarBtn() {
        //alert(JSON.stringify(this.state.usuario));

        if (this.state.btnAdd === false) {
            return (null);
        }

        return (
            <View>
                <Fab
                    active={false}
                    direction="up"
                    containerStyle={{}}
                    style={{backgroundColor: '#4d83e7'}}
                    position="bottomRight"
                    onPress={() => this.vistaRegistroInvitado()}>
                    <Icon name="md-add"/>
                </Fab>
            </View>
        )
    }

    vistaRegistroInvitado() {
        console.log('Btn press');
        this.props.navigation.push('RegistroInvitado');
    }

    GetGridViewItem(item) {
        this.props.navigation.push(item);
    }

    getLastPredica = async () => {

        const value1 = await this.Noticias.getUltimaNoticia();
        if (JSON.stringify(value1.noticia) === JSON.stringify(this.state.noticias)) {
            console.log("igual inicio");
            this._isMounted ? this.setState({cargaNoticias: false}) : null
        } else {
            this._isMounted ? this.setState({noticias: value1.noticia, cargaNoticias: false}) : null;
            console.log("cambio inicio");
        }

    };

    componentWillUnmount() {
        this._isMounted = false;

    }

    cargarNoticias(navigation) {
        //console.log(JSON.stringify(this.state.noticias))
        if (this.state.cargaNoticias)
            return (<Spinner color='blue'/>);
        else
            return (
                <FlatList
                    style={styles.listItem}
                    data={this.state.noticias}
                    extraData={this.state}
                    keyExtractor={(item, index) => String(index)}
                    renderItem={({item, index}) => {
                        return (
                            <ListItem
                                style={styles.cardContainer}
                                avatar
                                onPress={() => this.props.navigation.push('InfoNoticia', {noticia: item})}
                                selected={this.state.selected === item.titulo}
                            >
                                <Content>
                                    <Card>

                                        <CardItem style={styles.cardHeader}>
                                            <Left>
                                                <Thumbnail
                                                    source={{uri: "https://gospelapp.lasbienaventuranzas.org/images/perfil/" + item.imagen}}/>
                                                <Body>
                                                    <Text>{this.capitalize(item.titulo)}</Text>
                                                    <Text
                                                        note>{this.capitalize(item.autor.nombres + " " + item.autor.apellidos)}
                                                    </Text>
                                                    <Text note>{item.fecha}</Text>
                                                </Body>
                                            </Left>
                                        </CardItem>
                                        <CardItem style={styles.cardOptions}>

                                            <Left>
                                                <Button transparent>
                                                    {
                                                        item.like ? <Icon active name="md-heart"/> :
                                                            <Icon active name="md-heart-empty"/>
                                                    }

                                                    <Text>{item.likes}</Text>
                                                </Button>

                                            </Left>

                                            <Right>
                                                <Button transparent>
                                                    <Icon active name="eye"/>
                                                    <Text>{item.vistos}</Text>
                                                </Button>
                                            </Right>

                                        </CardItem>
                                    </Card>
                                </Content>
                            </ListItem>

                        );
                    }}
                />);
    }

    capitalize(s) {
        return s.toLowerCase().replace(/\b./g, function (a) {
            return a.toUpperCase();
        });
    };

    render() {
        return (
            <Container>
                <Header style={{height: StatusBar.currentHeight}}/>
                <Content style={{backgroundColor: "#ffffff"}}>
                    {this.renderTab(this.state.footerTab)}
                </Content>


                {this.cargarBtn()}


                <Footer>
                    <FooterTab>
                        <Button vertical active={this.state.footerTab === 1} onPress={() => {
                            this.changeTab(1)
                        }}>
                            <Icon name="apps"/>
                            <Text>Inicio</Text>
                        </Button>
                        <Button vertical active={this.state.footerTab === 2} onPress={() => {
                            this.changeTab(2)
                        }}>
                            <Icon name="list-box"/>
                            <Text>Noticias</Text>
                        </Button>
                        <Button vertical active={this.state.footerTab === 3} onPress={() => {
                            this.changeTab(3)
                        }}>
                            <Icon name="people"/>
                            <Text>Invitados</Text>
                        </Button>
                        <Button vertical active={this.state.footerTab === 4} onPress={() => {
                            this.changeTab(4)
                        }}>
                            <Icon name="person"/>
                            <Text>Cuenta</Text>
                        </Button>
                    </FooterTab>
                </Footer>
            </Container>
        );
    }
}

const styles = StyleSheet.create({

    lastPredicaContainer: {
        paddingRight: 15
    },

    lastPredicaTitle: {
        textAlign: 'center',
        fontWeight: 'bold',
        paddingBottom: 15,
        paddingTop: 15,
        fontSize: 20,
    },


    logoContainer: {
        //alignItems: 'center',
        //marginTop: 20
    },

    logoText: {
        color: '#000000',
        fontSize: 20,
        fontWeight: '500',
        marginTop: 10,
        marginBottom: 10,
        opacity: 0.5,
    },

    title: {
        marginLeft: 10,
    },

    mensaje: {
        margin: 10,
    },

    MainContainer: {

        justifyContent: 'center',
        flex: 1,
        margin: 10,
        paddingTop: (Platform.OS) === 'ios' ? 20 : 0

    },

    container: {

        justifyContent: 'center',
        flex: 1,
        alignItems: 'center',

    },

    GridViewBlockStyle: {

        justifyContent: 'center',
        flex: 1,
        alignItems: 'center',
        height: 120,
        width: '90%',
        marginTop: 10,
        marginLeft: 10,
        marginRight: 10,
        backgroundColor: '#4d83e7',
        borderRadius: 18,

    },

    GridViewInsideTextItemStyle: {

        color: '#000000',
        textAlign: 'center',
        marginTop: 5,
        fontSize: 15,
        justifyContent: 'center',

    },

    iconBtns: {
        color: 'rgb(255,255,255)',
        fontSize: 60,
    },
});