import React, {Component} from 'react';
import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Body,
    Icon,
    Text,
    Item, Label, Input, Textarea, Form, Toast, Thumbnail, ListItem
} from 'native-base';
import {Alert, StatusBar, View, FlatList, StyleSheet, AsyncStorage} from 'react-native'
import MethodsInvitados from '../methods/MethodsInvitados'

export default class RegistroInvitado extends Component {

    constructor() {
        super();
        this.Invitados = new MethodsInvitados();
        this.state = {
            result: true,
            nombre: null,
            apellido: null,
            telefono: null,
            direccion: null,
            oracion: null,
        };
    }


    componentDidMount() {

    }

    getResponse(result) {
console.log(result);
        if (result.success) {
            Toast.show({
                text: result.message,
                buttonText: "Ok",
                type: "success"
            });

            this.props.navigation.goBack()
        } else {

            Toast.show({
                text: result.message,
                buttonText: "Ok",
                type: "danger"
            });

        }

    }

    insertInvitado = () => {


        this.Invitados.addInvitado(
            this.state.nombre,
            this.state.apellido,
            this.state.telefono,
            this.state.direccion,
            this.state.oracion
        ).then(response =>

            this.getResponse(response)
        );


    };

    render() {
        return (
            <Container>
                <Header style={{height: StatusBar.currentHeight}}/>
                <Header>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon name="arrow-back"/>
                        </Button>
                    </Left>
                    <Body>
                        <Title>Nuevo Invitado</Title>
                    </Body>
                    <Right/>
                </Header>
                <Content>
                    <View style={styles.avatarContainer}>
                        <Thumbnail source={{ uri: "https://gospelapp.lasbienaventuranzas.org/images/perfil/avatar-1.png"  }} />
                    </View>
                    <ListItem itemDivider>
                        <Text size={5}>Datos del Invitado</Text>
                    </ListItem>
                    <Form>
                        <Item stackedLabel>
                            <Label>Nombres</Label>
                            <Input
                                onChangeText={TextInputValue => this.setState({nombre: TextInputValue})}
                            />
                        </Item>

                        <Item stackedLabel>
                            <Label>Apellidos</Label>
                            <Input
                                onChangeText={TextInputValue => this.setState({apellido: TextInputValue})}
                            />
                        </Item>

                        <Item stackedLabel>
                            <Label>Telefono</Label>
                            <Input
                                onChangeText={TextInputValue => this.setState({telefono: TextInputValue})}
                                keyboardType={'numeric'}
                            />
                        </Item>

                        <Item stackedLabel>
                            <Label>Direccion</Label>
                            <Input
                                onChangeText={TextInputValue => this.setState({direccion: TextInputValue})}
                            />
                        </Item>

                        <Item stackedLabel>
                            <Label>Motivo de Oracion</Label>
                            <Input
                                multiline={true}
                                numberOfLines={5}
                                onChangeText={TextInputValue => this.setState({oracion: TextInputValue})}
                            />
                        </Item>

                    </Form>
                    <View style={styles.btnContainer}>

                        <Button style={styles.btns}
                                onPress={this.insertInvitado}
                        >
                            <Text>Guardar</Text>
                        </Button>

                        <Button style={styles.btns} danger onPress={() => {
                            this.props.navigation.goBack()
                        }}>
                            <Text style={styles.textBtns}>Cancelar</Text>
                        </Button>

                    </View>
                </Content>

            </Container>
        );
    }
}

const styles = StyleSheet.create({

    btnContainer: {
        flexDirection: "row",
        marginTop: 15,
    },

    btns: {
        width: '45%',
        marginLeft: '2.5%',
        marginRight: '2.5%',
        justifyContent: 'center',
    },

    textarea: {
        borderBottomWidth: 1,
        borderBottomColor: '#dedede',
        marginBottom: 10,
        paddingLeft: 5,
        paddingRight: 5
    },
    avatarContainer: {
        alignItems: 'center',
        marginTop: 20,
        marginBottom: 20
    },

});