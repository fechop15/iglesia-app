import React, {Component} from 'react';
import {
    Container,
    Body,
    Text,
    ListItem, Toast, View, FooterTab, Footer, Header, Left, Button, Icon, Title, Right
} from 'native-base';
import {StatusBar, StyleSheet, WebView} from 'react-native'
import MethodsNoticias from "../methods/MethodsNoticias";


export default class InfoNoticia extends Component {


    constructor(props) {
        super(props);
        this.Noticias = new MethodsNoticias();
        this.state = {
            noticia: {
                autor: [],
                id: "",
                contenido: "",
                like: "",
                estado: "",
                fecha: "",
                imagen: "",
                tipo: "",
                noticia: "",
                titulo: "",
                vistos: ""
            },
        }
    }

    async componentDidMount() {
        await this.setState({noticia: (this.props.navigation.getParam('noticia'))});


        this.visitaNoticia();
    }


    visitaNoticia = async () => {
        await this.Noticias.visitaNoticia(this.state.noticia.id, null);
    };

    likeDislike = async () => {
        {

            let like = !(this.state.noticia.like);

            //console.log('Valor del like: ' + this.state.noticia.like)

            this.setState(prevState => ({
                ...prevState,
                noticia: {
                    ...prevState.noticia,
                    like: like
                }
            }));

            await this.Noticias.likeNoticia(this.state.noticia.id, like);

            this.iconLike();

            //console.log(this.state.noticia.like)

        }
    };

    iconLike() {
        if (this.state.noticia.like === 0 || this.state.noticia.like === false || this.state.noticia.like === null) {
            return <Icon name="md-heart-empty"/>
        } else {
            return <Icon name="md-heart"/>
        }
    };


    render() {

        const html =
            '<h2 style="text-align: center;">' + this.state.noticia.titulo + '</h2>' +
            '<p style="text-align: left; font-style: italic;"> Escrito por: <strong>' + this.state.noticia.autor.nombres + ' '+this.state.noticia.autor.apellidos + '</strong><br> ' + this.state.noticia.fecha + ' </p>' +
            '<br>' +
            '<br>' +
            '' + this.state.noticia.contenido + '';

        return (


            <Container>
                <Header style={{height: StatusBar.currentHeight}}/>
                <Header>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon name="arrow-back"/>
                        </Button>
                    </Left>
                    <Body>
                        <Title>{this.state.noticia.tipo}</Title>
                    </Body>

                    <Right>
                        <Button transparent onPress={() => this.likeDislike()}>

                            {this.iconLike()}

                        </Button>
                    </Right>

                </Header>

                <View style={{flex: 1}}>
                    <WebView
                        javaScriptEnabled={true}
                        style={styles.webContainer}
                        originWhitelist={['*']}
                        source={{html: html}}
                    />
                </View>

                <Text>

                </Text>

            </Container>
        );
    }
}

const styles = StyleSheet.create({

    webContainer: {
        flex: 1,
        backgroundColor: 'rgba(255,255,255,0)',
    },

});