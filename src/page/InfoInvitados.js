import React, {Component} from 'react';
import {
    Container,
    Header,
    Title,
    Content,
    Card,
    CardItem,
    Button,
    Left,
    Right,
    Body,
    Icon,
    Text,
    Separator,
    ListItem, Toast, Thumbnail, FooterTab, Footer
} from 'native-base';
import {StatusBar, StyleSheet, View} from 'react-native'
import MethodsInvitados from '../methods/MethodsInvitados'

export default class InfoInvitados extends Component {


    constructor(props) {
        super(props);
        this.Invitados = new MethodsInvitados();
        this.state = {
            invitado: {
                id: "",
                nombres: "",
                apellidos: "",
                fecha_registro: "",
                rol: "",
                telefono: "",
                direccion: "",
                estado: "",
                invitador: "",
                imagen: ""
            },
            cargo:'Miembro',
        }
    }

    componentDidMount() {

        this.setState({invitado: (this.props.navigation.getParam('invitado'))});
        this.setState({cargo: (this.props.navigation.getParam('cargo'))});
        console.log(this.props.navigation.getParam('invitado'));
    }

    confirmarInvitao = () => {

        this.Invitados.confirmarInvitado(this.state.invitado.id).then(
            response => this.getResponse(response)
        );

    };

    eliminarInvitado = () => {

        this.Invitados.eliminarInvitado(this.state.invitado.id).then(
            response => this.getResponse(response)
        );

    };

    getResponse(result) {
        if (result.success) {
            Toast.show({
                text: result.message,
                buttonText: "Ok",
                type: "success",
                position: "top"
            });

            this.props.navigation.goBack()
        } else {

            Toast.show({
                text: result.message,
                buttonText: "Ok",
                type: "danger",
                position: "top"
            });

        }

    }

    render() {
        return (
            <Container>
                <Header style={{height: StatusBar.currentHeight}}/>
                <Header>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon name="arrow-back"/>
                        </Button>
                    </Left>
                    <Body>
                        <Title>Invitado</Title>
                    </Body>
                    <Right/>
                </Header>
                <Content>
                    <ListItem itemDivider>
                        <Text size={5}>Datos del Invitado</Text>
                    </ListItem>
                    <ListItem>
                        <Body>
                            <Text note>Nombres:</Text>
                            <Text>{this.state.invitado.nombres}</Text>
                        </Body>
                    </ListItem>
                    <ListItem>
                        <Body>
                            <Text note>Apellidos:</Text>
                            <Text>{this.state.invitado.apellidos}</Text>
                        </Body>
                    </ListItem>

                    <ListItem last>
                        <Body>
                            <Text note>Telefono:</Text>
                            <Text>{this.state.invitado.telefono}</Text>
                        </Body>
                    </ListItem>

                    <ListItem last>
                        <Body>
                            <Text note>Direccion:</Text>
                            <Text>{this.state.invitado.direccion}</Text>
                        </Body>
                    </ListItem>

                    <ListItem last>
                        <Body>
                            <Text note>Motivo de oración:</Text>
                            <Text>{this.state.invitado.oracion}</Text>
                        </Body>
                    </ListItem>

                    <ListItem last>
                        <Body>
                            <Text note>Fecha Registro:</Text>
                            <Text>{this.state.invitado.fecha_registro}</Text>
                        </Body>
                    </ListItem>


                    <ListItem last>
                        <Body>
                            <Text note>Estado:</Text>
                            <Text>{
                                this.state.invitado.estado === 1? 'Confirmado' : 'Por confirmar'
                            }
                            </Text>
                        </Body>
                    </ListItem>


                    <View style={styles.btnContainer}>

                        {this.state.invitado.estado === 0?
                            this.state.cargo==='Pastor'?
                                <Button style={styles.btns} success
                                        onPress={this.confirmarInvitao}
                                >
                                    <Text>Confirmar</Text>
                                </Button>:null
                                :null
                        }

                        {this.state.invitado.estado === 0?
                            <Button style={styles.btns} danger onPress={() => {
                                this.eliminarInvitado()
                            }}>
                                <Text style={styles.textBtns}>Eliminar</Text>
                            </Button>:null
                        }

                    </View>



                </Content>
                <Card>
                    <CardItem>
                        <Left>
                            <Thumbnail
                                source={{uri: "https://gospelapp.lasbienaventuranzas.org/images/perfil/" + this.state.invitado.imagen}}/>
                            <Body>
                                <Text note>Invitador</Text>
                                <Text>{this.state.invitado.invitador}</Text>
                            </Body>
                        </Left>
                    </CardItem>
                </Card>
            </Container>
        );
    }
}

const styles = StyleSheet.create({

    optionCuenta: {
        color: '#495fdc',
    },

    btnContainer: {
        flexDirection: "row",
        marginTop: 15,
        justifyContent: 'center',
    },

    btns: {
        width: '45%',
        marginLeft: '2.5%',
        marginRight: '2.5%',
        justifyContent: 'center',
    },


});