import React, {Component} from 'react';
import {
    Toast,
    ScrollableTab,
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Body,
    Icon,
    Text,
    List,
    Item,
    Input,
    View,
    Tabs, ListItem, Spinner, Card, CardItem, Thumbnail
} from 'native-base';
import {AsyncStorage, FlatList} from "react-native";

export default class MisInvitados extends Component {

    _isMounted = false;

    constructor(props) {
        super(props);
        this.state = {
            misInvitados: [],
            cargando: true,
            data: [],
            text: ''
        };

    }

    componentDidMount() {
        this._isMounted = true;
        this.getMisInvitados();
        this.props.navigation.addListener('didFocus', () => {
            this._isMounted ? this.getMisInvitados() : null;
        });
        console.log('mis invitados cargado')
    }

    componentWillUnmount() {
        this._isMounted = false;
        console.log('mis invitados desmontado')

    }

    getMisInvitados = async () => {
        console.log("cargando mis invitados");

        let api_token = "";
        await AsyncStorage.getItem('user')
            .then((value) => {
                    api_token = JSON.parse(value).api_token;
                }
            );
        fetch('https://gospelapp.lasbienaventuranzas.org/api/v1/getMisInvitados', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                'api_token': api_token,
            }),
        }).then((response) => response.json())
            .then((res) => {
                console.log(res);
                if (res.message === 'Unauthenticated.') {
                    this.cerrarSession();
                } else {
                    this._isMounted ? this.setState({
                        misInvitados: res.invitados,
                        cargando: false,
                        data: res.invitados
                    }) : null;
                    //this.setState({cargando: false});
                }

            }).done();
    };

    cargarUsuarios(navigation) {
        if (this.state.cargando)
            return (<Spinner color='blue'/>);
        else
            return (<FlatList
                data={this.state.misInvitados}
                extraData={this.state}
                keyExtractor={(item, index) => String(index)}
                renderItem={({item, index}) => {
                    return (
                        <ListItem avatar
                                  selected={this.state.selected === item.nombre}
                                  onPress={() => {

                                      this.setState({selected: item});
                                      navigation.push('InfoInvitados',
                                          {
                                              invitado: item,
                                              navigation: navigation,
                                              cargo:'Miembro'
                                          }
                                      );

                                  }}


                        >

                            <Left>
                                <Thumbnail
                                    source={{uri: "https://gospelapp.lasbienaventuranzas.org/images/perfil/avatar-1.png"}}/>
                            </Left>
                            <Body>
                                <Text>{item.nombres} {item.apellidos}</Text>
                                <Text note>{item.fecha_registro}</Text>
                            </Body>
                            <Right>
                                <Icon name="arrow-forward"/>
                            </Right>

                        </ListItem>
                    );
                }}
            />);
    }

    cerrarSession = async () => {
        // await AsyncStorage.clear();
        // const resetAction = StackActions.reset({
        //     index: 0,
        //     actions: [NavigationActions.navigate({routeName: 'Login'})],
        // });
        // this.props.navigation.dispatch(resetAction);

    };

    searchFilterFunction(text) {
        const newData = this.state.misInvitados.filter(item => {
            const itemData = `${item.nombre.toUpperCase()}`;
            const textData = text.toUpperCase();

            return itemData.indexOf(textData) > -1;
        });

        this.setState({
            data: newData,
            text: text
        });

    };

    render() {
        const {navigation} = this.props;

        return (
            <Container>
                <Header>
                    <Body>
                        <Title>Mis Invitados</Title>
                    </Body>
                    <Right>
                    </Right>
                </Header>

                <Header noShadow searchBar rounded>
                    <Item>
                        <Icon name="ios-search"/>
                        <Input
                            onChangeText={(text) => this.searchFilterFunction(text)}
                            value={this.state.text}
                            placeholder="Buscar.."
                        />
                        <Icon name="ios-people"/>
                    </Item>
                    <Button transparent>
                        <Text>Buscar</Text>
                    </Button>
                </Header>

                <Content>
                    {this.cargarUsuarios(navigation)}
                </Content>

            </Container>
        );
    }
}