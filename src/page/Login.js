import React from 'react';
import {
    StyleSheet,
    ImageBackground,
    Text,
    View,
    Image,
    TextInput,
    Dimensions,
    TouchableOpacity,
    StatusBar,
    AsyncStorage, FlatList
} from 'react-native';
import bgimagen from '../../assets/bg2.gif';
import logo from '../../assets/icon-app.png';
import {Button, Icon, Spinner, Toast} from "native-base";
import {Notifications} from "expo";
import * as Permissions from "expo-permissions";

const {width: WIDTH} = Dimensions.get('window');

async function registerForPushNotificationsAsync() {
    const { status: existingStatus } = await Permissions.getAsync(
        Permissions.NOTIFICATIONS
    );
    let finalStatus = existingStatus;

    // only ask if permissions have not already been determined, because
    // iOS won't necessarily prompt the user a second time.
    if (existingStatus !== 'granted') {
        // Android remote notification permissions are granted during the app
        // install, so this will only ask on iOS
        const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
        finalStatus = status;
    }

    // Stop here if the user did not grant permissions
    if (finalStatus !== 'granted') {
        return;
    }

    // Get the token that uniquely identifies this device
    let token = await Notifications.getExpoPushTokenAsync();

    //console.log(token);
    // POST the token to your backend server from where you can retrieve it to send push notifications.
    /* return fetch(PUSH_ENDPOINT, {
       method: 'POST',
       headers: {
         Accept: 'application/json',
         'Content-Type': 'application/json',
       },
       body: JSON.stringify({
         token: {
           value: token,
         },
         user: {
           username: 'Brent',
         },
       }),
     });*/
    return token;
}

export default class Login extends React.Component {

    static navigationOptions = {
        header: null
    };

    constructor() {
        super();
        this.state = {
            showPass: true,
            username: '',
            password: '',
            cargando: true,
            notification:{},
        };
    }

    _handleNotification = (notification) => {
        //this.setState({notification: notification});
        console.log(notification);
    };

    async componentDidMount() {
        const {status: existingStatus} = await Permissions.getAsync(
            Permissions.NOTIFICATIONS
        );
        let finalStatus = existingStatus;

        // only ask if permissions have not already been determined, because
        // iOS won't necessarily prompt the user a second time.
        if (existingStatus !== 'granted') {
            // Android remote notification permissions are granted during the app
            // install, so this will only ask on iOS
            const {status} = await Permissions.askAsync(Permissions.NOTIFICATIONS);
            finalStatus = status;
        }

        // Stop here if the user did not grant permissions
        if (finalStatus !== 'granted') {
            return;
        }

        // Get the token that uniquely identifies this device
        let tokenNotification = await Notifications.getExpoPushTokenAsync();

        console.log(tokenNotification);
        AsyncStorage.getItem('user')
            .then((value) => {
                    if (value != null) {
                        //console.log(value);

                        fetch('https://gospelapp.lasbienaventuranzas.org/api/v1/update', {
                            method: 'POST',
                            headers: {
                                Accept: 'application/json',
                                'Content-Type': 'application/json',
                            },
                            body: JSON.stringify({
                                'api_token': JSON.parse(value).api_token,
                                'notificacion': tokenNotification,
                            }),
                        }).then((response) => response.json())
                            .then((res) => {
                                //console.log(JSON.parse(value).api_token);
                                if (res.message === 'Unauthenticated.') {
                                    AsyncStorage.clear();
                                    this.setState({cargando: false})
                                } else {
                                    AsyncStorage.setItem('user', JSON.stringify(res.userData));
                                    this.props.navigation.replace('Inicio', {usuario: value});
                                }
                            }).done();
                    } else {
                        this.setState({cargando: false})
                    }

                }
            );

        Notifications.addListener(this._handleNotification);
    }

    showPass = () => {
        var temp = !this.state.showPass;
        this.setState({showPass: temp})
    };

    ingresar = () => {

        fetch('https://gospelapp.lasbienaventuranzas.org/api/login', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                'email': this.state.username,
                'password': this.state.password,
            }),
        }).then((response) => response.json())
            .then((res) => {

                //console.log(res);

                if (res.success === true) {
                    AsyncStorage.setItem('user', JSON.stringify(res.userData));
                    this.componentDidMount();

                    //this.props.navigation.replace('Inicio', {usuario: res.userData});
                } else {
                    Toast.show({
                        text: res.message,
                        buttonText: "Ok",
                        type: "danger"
                    });
                }

            })
            .catch(error => console.error('Error:', error)).done();
    };

    cargarLogin () {
        if (this.state.cargando)
            return (<Spinner color='green'/>);
        else
            return (
                <View>
                    <View style={styles.inputContainer}>
                        <Icon style={styles.icono} name={'person'} size={20}/>
                        <TextInput style={styles.input}
                                   placeholder={'Usuario'}
                                   placeholderTextColor={'rgba(255,255,255,0.9)'}
                                   underLineColorAndroid='transparent'
                                   onChangeText={(username) => this.setState({username})}
                                   keyboardType={'email-address'}
                        />

                    </View>

                    <View style={styles.inputContainer}>
                        <Icon style={styles.icono} name={'lock'} size={20}/>
                        <TextInput style={styles.input}
                                   placeholder={'Password'}
                                   secureTextEntry={this.state.showPass}
                                   placeholderTextColor={'rgba(255,255,255,0.9)'}
                                   underLineColorAndroid='transparent'
                                   onChangeText={(password) => this.setState({password})}
                        />
                        <TouchableOpacity style={styles.btnEye}>
                            <Icon
                                type={'FontAwesome'}
                                name={this.state.showPass ? 'eye' : 'eye-slash'}
                                style={styles.btnEyeIcon}
                                onPress={this.showPass.bind(this)}
                                size={20}/>
                        </TouchableOpacity>
                    </View>

                    <Button block rounded style={styles.btnLogin} onPress={this.ingresar.bind(this)}>
                        <Text style={styles.text}>Ingresar</Text>
                    </Button>
                </View>
            );
    };

    render() {
        return (

            <ImageBackground source={bgimagen} style={styles.containerImage}>
                <StatusBar
                    barStyle="default"
                    // dark-content, light-content and default
                    hidden={false}
                    //To hide statusBar
                    backgroundColor="transparent"
                    //Background color of statusBar
                    translucent={true}
                    //allowing light, but not detailed shapes
                    networkActivityIndicatorVisible={true}
                />
                <View style={styles.logoContainer}>
                    <Image source={logo} style={styles.logo}/>
                    <Text style={styles.logoText}> GospelApp</Text>
                </View>


                {this.cargarLogin()}


            </ImageBackground>
        );
    }
}
//style={styles.btnLogin}
const styles = StyleSheet.create({
    containerImage: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        height: '100%',
    },
    logoContainer: {
        alignItems: 'center',
    },
    logoText: {
        color: 'rgba(255,255,255,0.91)',
        fontSize: 20,
        fontWeight: '500',
        marginTop: 10,
        marginBottom: 10,

    },
    logo: {
        width: 150,
        height: 135,
    },
    input: {
        width: WIDTH - 55,
        height: 45,
        borderRadius: 25,
        fontSize: 16,
        paddingLeft: 45,
        backgroundColor: 'rgba(160,160,160,0.5)',
        color: 'rgba(255,255,255,0.7)',
        marginHorizontal: 25
    },
    inputContainer: {
        marginBottom: 10,
    },
    icono: {
        position: 'absolute',
        top: 8,
        left: 37,
        color: 'rgba(255,255,255,0.9)',
    },
    btnEye: {
        position: 'absolute',
        top: 8,
        color: 'rgba(255,255,255,0.9)',
        right: 37,
    },
    btnEyeIcon: {
        color: 'rgba(255,255,255,0.9)',
    },
    btnLogin: {
        //width: WIDTH - 55,
        //height: 45,
        //borderRadius: 25,
        backgroundColor: 'rgb(20,99,10)',
        //justifyContent: 'center',
        marginTop: 10,
        marginHorizontal: 25,
    },
    text: {
        color: 'rgba(255,255,255,0.9)',
        fontSize: 16,
        textAlign: 'center'
    }
});