import React, {Component} from 'react';
import {
    Container,
    Header,
    Title,
    Content,
    Button,
    Left,
    Right,
    Body,
    Icon,
    Text,
    Item,
    Input,
    ListItem, Spinner, Thumbnail
} from 'native-base';
import {AsyncStorage, FlatList, StatusBar} from "react-native";
import MethodsUsuarios from "../methods/MethodsUsuarios";

export default class Usuarios extends Component {

    _isMounted = false;

    constructor(props) {
        super(props);
        this.Usuarios = new MethodsUsuarios();
        this.state = {
            usuarios: [],
            cargando: true,
            text: '',
            data: [],
        };
    }

    componentDidMount() {
        this._isMounted = true;

        this.props.navigation.addListener('didFocus', () => {
            this._isMounted ? this.getUsuarios() : null;
        });
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    getUsuarios = async () => {

        let api_token = "";
        await AsyncStorage.getItem('user')
            .then((value) => {
                    api_token = JSON.parse(value).api_token;
                }
            );
        fetch('https://gospelapp.lasbienaventuranzas.org/api/v1/getUsuarios', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                'api_token': api_token,
            }),
        }).then((response) => response.json())
            .then((res) => {
                console.log(res);
                if (res.message === 'Unauthenticated.') {
                    //salir
                } else {
                    this._isMounted ? this.setState({
                        usuarios: res.usuarios,
                        data: res.usuarios,
                        cargando: false
                    }) : null;
                }

            }).done();
    };

    cargarUsuarios() {
        if (this.state.cargando)
            return (<Spinner color='blue'/>);
        else
            return (<FlatList
                data={this.state.data}
                extraData={this.state}
                keyExtractor={(item, index) => String(index)}
                renderItem={({item, index}) => {
                    return (
                        <ListItem avatar
                                  selected={this.state.selected === item.nombre}
                                  onPress={() => {

                                      this.setState({selected: item});
                                      this.props.navigation.push('InfoUsuarios',
                                          {
                                              usuario: item,
                                              navigation: this.props.navigation
                                          }
                                      );

                                  }}
                        >
                            <Left>
                                <Thumbnail
                                    source={{uri: "https://gospelapp.lasbienaventuranzas.org/images/perfil/" + item.imagen}}/>
                            </Left>
                            <Body>
                                <Text>{item.nombre}</Text>
                                <Text note>{item.cargo}</Text>
                            </Body>
                            <Right>
                                <Icon name="arrow-forward"/>
                            </Right>

                        </ListItem>
                    );
                }}
            />);
    }

    searchFilterFunction(text) {
        const newData = this.state.usuarios.filter(item => {
            const itemData = `${item.nombre.toUpperCase()}`;
            const textData = text.toUpperCase();

            return itemData.indexOf(textData) > -1;
        });
        this._isMounted ? this.setState({data: newData, text: text}) : null;

    };

    render() {
        return (
            <Container>
                <Header style={{height: StatusBar.currentHeight}}/>
                <Header>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon name="arrow-back"/>
                        </Button>
                    </Left>
                    <Body>
                        <Title>Usuarios</Title>
                    </Body>
                    <Right>
                    </Right>
                </Header>

                <Header noShadow searchBar rounded>
                    <Item>
                        <Icon name="ios-search"/>
                        <Input
                            onChangeText={(text) => this.searchFilterFunction(text)}
                            value={this.state.text}
                            placeholder="Buscar.."
                        />
                        <Icon name="ios-people"/>
                    </Item>
                    <Button transparent>
                        <Text>Buscar</Text>
                    </Button>
                </Header>

                <Content>
                    {this.cargarUsuarios()}
                </Content>

            </Container>
        );
    }
}