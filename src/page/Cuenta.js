import React, {Component} from 'react';
import {
    Header,
    Title,
    Content,
    Left,
    Right,
    Body,
    Text,
    Thumbnail,
    View,
    ListItem
} from 'native-base';
import { StyleSheet, TouchableOpacity, AsyncStorage} from 'react-native'
import {StackActions, NavigationActions} from 'react-navigation';

export default class Cuenta extends Component {



    constructor(props) {
        super(props);

        this.state = {
            UserInfo:
                {
                    cedula: "",
                    nombre: "",
                    correo: "",
                    fecha: "",
                    rol: "",
                    telefono: "",
                    fecha_reg: "",
                    direccion: "",
                    api_token: ""
                },
        }
    }

    componentDidMount() {
        this._loadInitialState().done();
    }

    _loadInitialState = async () => {
        await AsyncStorage.getItem('user')
            .then((value) => {
                    this.setState({UserInfo: JSON.parse(value)});
                }
            );
    };

    static async cerrarSession (navigation) {
        await AsyncStorage.clear();
        const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({routeName: 'Login'})],
        });
        navigation.dispatch(resetAction);

    };

    render() {
        const { navigation } = this.props;

        return (

        <View>
                <Header>
                    <Left>
                    </Left>
                    <Body>
                        <Title>Cuenta</Title>
                    </Body>
                    <Right/>
                </Header>
                <Content>

                    <View style={styles.avatarContainer}>
                        <Thumbnail circle large
                                   source={{uri: "https://gospelapp.lasbienaventuranzas.org/images/perfil/" + this.state.UserInfo.imagen}}/>
                        <Text>{this.state.UserInfo.nombre}</Text>
                        <Text note>{this.state.UserInfo.cargo}</Text>
                    </View>

                    <ListItem itemDivider>
                        <Text size={5}>Datos de Usuario</Text>
                    </ListItem>

                    <ListItem last>
                        <Body>
                            <Text note>E-Mail:</Text>
                            <Text>{this.state.UserInfo.email}</Text>
                        </Body>
                    </ListItem>

                    <ListItem last>
                        <Body>
                            <Text note>Identificacion:</Text>
                            <Text>{this.state.UserInfo.cedula}</Text>
                        </Body>
                    </ListItem>

                    <ListItem last>
                        <Body>
                            <Text note>Fecha de nacimiento:</Text>
                            <Text>{this.state.UserInfo.fecha_nacimiento}</Text>
                        </Body>
                    </ListItem>

                    <ListItem last>
                        <Body>
                            <Text note>Telefono:</Text>
                            <Text>{this.state.UserInfo.telefono}</Text>
                        </Body>
                    </ListItem>

                    <ListItem last>
                        <Body>
                            <Text note>Rol:</Text>
                            <Text>{this.state.UserInfo.rol}</Text>
                        </Body>
                    </ListItem>



                    <ListItem itemDivider>
                        <Text size={5}>Cuenta</Text>
                    </ListItem>
                    <ListItem onPress={() => Cuenta.cerrarSession(navigation)} last>
                        <TouchableOpacity onPress={() => Cuenta.cerrarSession(navigation)}>
                            <Text style={styles.optionCuenta}>Cerrar Sesion</Text>
                        </TouchableOpacity>
                    </ListItem>
                </Content>
            </View>
        );
    }
}

const styles = StyleSheet.create({

    optionCuenta: {
        color: '#495fdc',
    },

    avatarContainer: {
        alignItems: 'center',
        marginTop: 20,
        marginBottom: 20
    },

});