import React, {Component} from 'react';
import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    ActionSheet,
    FooterTab,
    Button,
    Left,
    Right,
    Body,
    Icon,
    Text,
    DatePicker,
    Item, Label, Input, Textarea, Form, Toast
} from 'native-base';
import {
    Picker,
    Alert,
    StatusBar,
    View,
    FlatList,
    StyleSheet,
    AsyncStorage,
    TextInput,
    TouchableOpacity, ImageBackground
} from 'react-native'
import MethodsUsuarios from "../methods/MethodsUsuarios";

var tipoUsuarios = ["Administrador", "Invitador"];

export default class RegistroUsuario extends Component {

    constructor() {
        super();
        this.Usuarios = new MethodsUsuarios();
        this.state = {
            result: true,
            nombre: '',
            correo: '',
            fecha: '',
            rol: '',
            telefono: '',
            password: '',
            direccion: '',
        };
        this.setDate = this.setDate.bind(this);
    }


    componentDidMount() {

    }

    getResponse(result) {

        if (result.success) {
            Toast.show({
                text: result.message,
                buttonText: "Ok",
                type: "success"
            });

            this.props.navigation.goBack()
        } else {

            Toast.show({
                text: result.message,
                buttonText: "Ok",
                type: "danger"
            });

        }

    }

    insertUsuario = () => {

        let app = this.state;

        this.Usuarios.addUsuario(
            app.nombre,
            app.correo,
            app.fecha,
            app.rol,
            app.telefono,
            app.password,
            app.direccion
        ).then(response =>

            this.getResponse(response)
        );


    };

    getResponse(result) {

        if (result.success) {
            Toast.show({
                text: result.message,
                buttonText: "Ok",
                type: "success"
            });

            this.props.navigation.goBack()
        } else {

            Toast.show({
                text: result.message,
                buttonText: "Ok",
                type: "danger"
            });

        }

    }

    setDate(newDate) {
        this.setState({fecha: newDate});
    }


    render() {
        return (
            <Container>
                <Header>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon name="arrow-back"/>
                        </Button>
                    </Left>
                    <Body>
                        <Title>Registro de usuario</Title>
                    </Body>
                    <Right/>
                </Header>
                <Content>
                    <Form>

                        <Item stackedLabel>
                            <Label>Nombre</Label>
                            <Input
                                onChangeText={TextInputValue => this.setState({nombre: TextInputValue})}
                            />
                        </Item>

                        <Item stackedLabel>
                            <Label>Correo</Label>
                            <Input
                                onChangeText={TextInputValue => this.setState({correo: TextInputValue})}
                            />
                        </Item>

                        <Item fixedLabel style={styles.dateContainer1}>

                            <Label style={styles.label1}>
                                Fecha de nacimiento
                            </Label>

                            <Content style={styles.label2}>
                                <DatePicker
                                    style={styles.datePicker}
                                    defaultDate={Date.now()}
                                    minimumDate={new Date(1930, 1, 1)}
                                    locale={"es"}
                                    timeZoneOffsetInMinutes={undefined}
                                    modalTransparent={false}
                                    animationType={"fade"}
                                    androidMode={"calendar"}
                                    placeHolderText="Seleccionar"
                                    placeHolderTextStyle={{color: "#d3d3d3"}}
                                    onDateChange={this.setDate}
                                    disabled={false}
                                />
                            </Content>
                        </Item>


                        <Item stackedLabel>
                            <Label>Rol</Label>
                            <Picker
                                selectedValue={this.state.rol}
                                style={{height: 50, width: '100%', marginTop: '2.5%', marginBottom: '2.5%'}}
                                onValueChange={(itemValue, itemIndex) =>
                                    this.setState({rol: itemValue})
                                }>
                                <Picker.Item label="Invitador" value="Invitador"/>
                                <Picker.Item label="Administrador" value="Administrador"/>
                            </Picker>
                        </Item>


                        <Item stackedLabel>
                            <Label>Telefono</Label>
                            <Input
                                onChangeText={TextInputValue => this.setState({telefono: TextInputValue})}
                            />
                        </Item>

                        <Item stackedLabel>

                            <Label>
                                Contraseña
                            </Label>

                            <Input
                                style={styles.inputPw}
                                placeholder={'Password'}
                                secureTextEntry={true}
                                placeholderTextColor={'rgba(255,255,255,0.9)'}
                                underLineColorAndroid='transparent'
                                onChangeText={(password) => this.setState({password: password})}
                            />
                        </Item>

                        <Item stackedLabel>
                            <Label>Dirección</Label>
                            <Input
                                onChangeText={TextInputValue => this.setState({direccion: TextInputValue})}
                            />
                        </Item>

                    </Form>
                    <View style={styles.btnContainer}>

                        <Button style={styles.btns}
                                onPress={this.insertUsuario}
                        >
                            <Text>Guardar</Text>
                        </Button>

                        <Button style={styles.btns} danger onPress={() => {
                            this.props.navigation.goBack()
                        }}>
                            <Text style={styles.textBtns}>Cancelar</Text>
                        </Button>

                    </View>
                </Content>

            </Container>
        );
    }
}

const styles = StyleSheet.create({

    datePicker: {
        backgroundColor: '#2f00ff'
    },

    label1: {
        textAlign: 'left',
        justifyContent: 'flex-start',
        width: '100%'
    },


    label2: {
        textAlign: 'left',
        width: '102%',
        marginLeft: '2.5%',
        marginRight: '2.5%',
    },

    dateContainer1: {
        flexDirection: "column",
        justifyContent: 'flex-start',
    },


    btnContainer: {
        flexDirection: "row",
        marginTop: 15,
    },

    btns: {
        width: '45%',
        marginLeft: '2.5%',
        marginRight: '2.5%',
        justifyContent: 'center',
    },

    textarea: {
        borderBottomWidth: 1,
        borderBottomColor: '#dedede',
        marginBottom: 10,
        paddingLeft: 5,
        paddingRight: 5
    },

    btnEyeIcon: {
        color: 'rgba(0,0,0,0.9)',
    },

    containerPW: {
        flexDirection: 'column',
        width: '100%',
    },

    labelPw: {
        backgroundColor: '#ff0095',
        textAlign: 'left',
        width: '100%'
    },

    inputPw: {
        textAlign: 'left',
    },


    containerRow: {
        flexDirection: "column",
    }

});