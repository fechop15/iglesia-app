import React, {Component} from 'react';
import {
    ScrollableTab,
    Header,
    Content,
    View,
    Tab,
    Tabs
} from 'native-base';
import TabNoticias from './../component/tabs/tabNoticias';
import {StyleSheet} from "react-native";
import MethodsNoticias from "../methods/MethodsNoticias";

export default class Noticias extends Component {
    _isMounted = false;

    constructor(props) {
        super(props);
        this.Noticias = new MethodsNoticias();
        this.state = {
            noticias: [],
            eventos: [],
            cargaNoticias: true,
            cargaEventos: true,
        };

    }

    getNoticias = async () => {

        const value1 = await this.Noticias.fetchNoticias('predica');
        if (JSON.stringify(value1.noticias) === JSON.stringify(this.state.noticias)) {
            console.log("igual");
            this._isMounted ? this.setState({cargaNoticias: false}) : null
        } else {
            this._isMounted ? this.setState({noticias: value1.noticias, cargaNoticias: false}) : null;
            console.log("cambio");
        }

        const value2 = await this.Noticias.fetchNoticias('evento');
        if (JSON.stringify(value2.noticias) === JSON.stringify(this.state.eventos)) {
            console.log("igual");
            this._isMounted ? this.setState({cargaEventos: false}) : null
        } else {
            this._isMounted ? this.setState({eventos: value2.noticias, cargaEventos: false}) : null;
            console.log("cambio");
        }

    };

    componentDidMount() {

        this._isMounted = true;
        this.getNoticias();
        this.props.navigation.addListener('didFocus', () => {
            this._isMounted ? this.setState({cargaNoticias: true, cargaEventos: true}) : null;
            this._isMounted ? this.getNoticias() : null;
        });


    }

    componentWillUnmount() {
        this._isMounted = false;

    }


    render() {

        const {navigation} = this.props;

        return (
            <View hasTabs>
                <Header style={styles.header}/>

                <Content>
                    <Tabs onChangeTab={() => this.getNoticias()} renderTabBar={() => <ScrollableTab/>}>


                        <Tab heading="Eventos">
                            <TabNoticias
                                noticias={this.state.eventos}
                                cargando={this.state.cargaEventos}
                                navigation={navigation}
                            />
                        </Tab>

                        <Tab heading="Predica">
                            <TabNoticias
                                noticias={this.state.noticias}
                                cargando={this.state.cargaNoticias}
                                navigation={navigation}
                            />

                        </Tab>

                    </Tabs>

                </Content>


            </View>
        );
    }
}

const styles = StyleSheet.create({

    header: {
        height: 0,
    },


});