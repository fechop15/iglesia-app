import React, {Component} from 'react';
import {
    Container,
    Header,
    Title,
    Content,
    Button,
    Left,
    Right,
    Body,
    Icon,
    Text,
    ListItem,
    Thumbnail
} from 'native-base';
import {StatusBar, StyleSheet, View} from 'react-native'

export default class InfoUsuarios extends Component {

    constructor(props) {
        super(props);
        this.state = {
            UserInfo:
                {
                    cedula: "",
                    nombre: "",
                    correo: "",
                    fecha: "",
                    rol: "",
                    telefono: "",
                    fecha_nacimiento: "",
                    fecha_reg: "",
                    direccion: "",
                    tipo_sangre: "",
                    api_token: ""
                },
            isDateTimePickerVisible: false,
            dia: 1,
            mes: 1,
            anio: 2000
        };
    }

    componentDidMount() {
        this.setState({UserInfo: (this.props.navigation.getParam('usuario'))})
    }


    render() {

        return (

            <Container>
                <Header style={{height: StatusBar.currentHeight}}/>
                <Header>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon name="arrow-back"/>
                        </Button>
                    </Left>
                    <Body>
                        <Title>Usuario</Title>
                    </Body>
                    <Right/>
                </Header>
                <Content>
                    <View style={styles.avatarContainer}>
                        <Thumbnail circle large
                                   source={{uri: "https://gospelapp.lasbienaventuranzas.org/images/perfil/" + this.state.UserInfo.imagen}}/>
                        <Text>{this.state.UserInfo.nombre}</Text>
                        <Text note>{this.state.UserInfo.cargo}</Text>

                    </View>

                    <ListItem itemDivider>
                        <Text size={5}>Datos de Usuario</Text>
                    </ListItem>
                    <ListItem last>
                        <Body>
                            <Text note>E-Mail:</Text>
                            <Text>{this.state.UserInfo.email}</Text>
                        </Body>
                    </ListItem>

                    <ListItem last>
                        <Body>
                            <Text note>Tipo De Sangre:</Text>
                            <Text>{this.state.UserInfo.tipo_sangre}</Text>
                        </Body>
                    </ListItem>

                    <ListItem last>
                        <Body>
                            <Text note>Fecha de nacimiento:</Text>
                            <Text>{this.state.UserInfo.fecha_nacimiento}</Text>
                        </Body>
                    </ListItem>

                    <ListItem last>
                        <Body>
                            <Text note>Telefono:</Text>
                            <Text>{this.state.UserInfo.telefono}</Text>
                        </Body>
                    </ListItem>

                    <ListItem last>
                        <Body>
                            <Text note>Rol:</Text>
                            <Text>{this.state.UserInfo.rol}</Text>
                        </Body>
                    </ListItem>

                </Content>
            </Container>
        )
            ;
    }
}

const styles = StyleSheet.create({

    optionCuenta: {
        color: '#495fdc',
    },

    avatarContainer: {
        alignItems: 'center',
        marginTop: 20,
        marginBottom: 20
    },


});