import React, {Component} from 'react';
import {
    ScrollableTab,
    Header,
    Content,
    View,
    Tab,
    Tabs
} from 'native-base';
import TabInvitados from './../component/tabs/tabInvitados';
import {StyleSheet} from "react-native";
import MethodsInvitados from "../methods/MethodsInvitados";

export default class Invitados extends Component {
    _isMounted = false;

    constructor(props) {
        super(props);
        this.Invitados = new MethodsInvitados();
        this.state = {
            invitados: [],
            invitadosConfirmados: [],
            cargaInvidatos: true,
            cargaInvidatosConfirmados: true,
            active: true
        };

    }


    getInvitados = async () => {
        const value1 = await this.Invitados.fetchInvitados('0');
        //console.log(value1.invitados);
        if (JSON.stringify(value1.invitados) === JSON.stringify(this.state.invitados)) {
            //console.log("igual");
            this._isMounted ? this.setState({cargaInvidatos: false}) : null;

        } else {
            this._isMounted ? this.setState({invitados: value1.invitados, cargaInvidatos: false}) : null;
            //console.log("cambio");
        }

        const value = await this.Invitados.fetchInvitados('1');
        if (JSON.stringify(value.invitados) === JSON.stringify(this.state.invitadosConfirmados)) {
            //console.log("igual");
            this._isMounted ? this.setState({cargaInvidatosConfirmados: false}) : null;
        } else {
            this._isMounted ? this.setState({
                invitadosConfirmados: value.invitados,
                cargaInvidatosConfirmados: false
            }) : null;
            //console.log("cambio");
        }
    };

    componentDidMount() {
        this._isMounted = true;
        this.getInvitados();
        this.props.navigation.addListener('didFocus', () => {
            this._isMounted ? this.setState({cargaInvidatos: true, cargaInvidatosConfirmados: true}) : null;
            this._isMounted ? this.getInvitados() : null;
        });
        console.log("montado")

    }

    componentWillUnmount() {
        this._isMounted = false;
        console.log("desmontado")
    }

    render() {
        const {navigation} = this.props;
        return (
            <View hasTabs>
                <Header style={styles.header}/>
                <Content>
                    <Tabs onChangeTab={() => this.getInvitados()} renderTabBar={() => <ScrollableTab/>}>
                        <Tab heading="Invitados">
                            <TabInvitados
                                invitados={this.state.invitados}
                                cargando={this.state.cargaInvidatos}
                                navigation={navigation}
                            />
                        </Tab>
                        <Tab heading="Todos">
                            <TabInvitados
                                invitados={this.state.invitadosConfirmados}
                                cargando={this.state.cargaInvidatosConfirmados}
                                navigation={navigation}
                            />
                        </Tab>
                    </Tabs>
                </Content>
            </View>
        );
    }
}

const styles = StyleSheet.create({

    header: {
        height: 0,
    },

});