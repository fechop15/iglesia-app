import React, {Component} from "react";
import {Root} from "native-base";
import {AppLoading} from "expo";
import Navigation from "./component/Navigator";
import * as Font from "expo-font";

export default class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {loading: true};
    }

    async componentWillMount() {
        await Font.loadAsync({
            Roboto: require("../node_modules/native-base/Fonts/Roboto.ttf"),
            Roboto_medium: require("../node_modules/native-base/Fonts/Roboto_medium.ttf"),
            ftawesome5: require("../node_modules/native-base/Fonts/FontAwesome5_Regular.ttf")
        });
        this.setState({loading: false});
    }

    render() {
        if (this.state.loading) {
            return (
                <Root>
                    <AppLoading/>
                </Root>
            );
        }
        return (
            <Root>
                <Navigation></Navigation>
            </Root>
        );
    }
}