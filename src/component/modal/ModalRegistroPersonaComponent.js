import React, {Component} from 'react';
import {
    Text,
    Content,
    Button,
    Form,
    Item,
    Label,
    Input,
    Textarea,
    Icon,
    Right,
    Left,
    Body,
    Title,
    Header,
    Toast,
    Root, Container,
} from 'native-base';
import {Alert, Modal, StyleSheet, View} from "react-native";
import MethodsInvitados from "../../methods/MethodsInvitados";

export default class ModalRegistroPersonaComponent extends Component {

    setModalVisible(visible) {

        if (!visible) {

            Toast.show({
                text: "Persona Guardada Con Exito!",
                buttonText: "Okay",
                type: "success"
            });
        }
        this.setState({modalVisible: visible});
    }

    response(visible) {

        this.setState({modalVisible: visible});
        this.props.callback(true);
    }

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            result: true,
            nombre: '',
            telefono: '',
            direccion: '',
            invitador: '',
            oracion: ''
        };
        this.Invitados = new MethodsInvitados();

    }

    insertInvitado = () => {

        this.Invitados.addInvitado(
            this.state.nombre,
            this.state.telefono,
            this.state.direccion,
            this.state.invitador,
            this.state.oracion
        );
        this.response(false);
    };


    render() {
        return (
            <View>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    style={styles.container}
                    onRequestClose={() => {
                        Alert.alert('Modal has been closed.');
                        this.setModalVisible(false);
                    }}>
                    <Header>
                        <Left>
                            <Button transparent onPress={() => this.setModalVisible(false)}>
                                <Icon name="arrow-back"/>
                            </Button>
                        </Left>
                        <Body center>
                            <Title> Registrar Persona</Title>
                        </Body>
                        <Right>
                        </Right>
                    </Header>

                    <View style={styles.viewForm}>
                        <View>
                            <Form>
                                <Item stackedLabel>
                                    <Label>Nombre</Label>
                                    <Input
                                        onChangeText={TextInputValue => this.setState({nombre: TextInputValue})}
                                    />
                                </Item>

                                <Item stackedLabel>
                                    <Label>Telefono</Label>
                                    <Input
                                        onChangeText={TextInputValue => this.setState({nombre: TextInputValue})}
                                    />
                                </Item>

                                <Item stackedLabel>
                                    <Label>Direccion</Label>
                                    <Input
                                        onChangeText={TextInputValue => this.setState({nombre: TextInputValue})}
                                    />
                                </Item>

                                <Item stackedLabel>
                                    <Label>Quien lo Invito</Label>
                                    <Input
                                        onChangeText={TextInputValue => this.setState({nombre: TextInputValue})}
                                    />
                                </Item>

                                <Item stackedLabel>
                                    <Label>Motivo de Oracion</Label>
                                    <Input
                                        multiline={true}
                                        numberOfLines={5}
                                        onChangeText={TextInputValue => this.setState({nombre: TextInputValue})}
                                    />
                                </Item>

                            </Form>
                            <View style={styles.btnContainer}>

                                <Button style={styles.btns}
                                        onPress={this.insertInvitado}
                                >
                                    <Text>Guardar</Text>
                                </Button>

                                <Button style={styles.btns} danger onPress={() => {
                                    this.response(false)
                                }}>
                                    <Text style={styles.textBtns}>Cancelar</Text>
                                </Button>

                            </View>

                        </View>

                    </View>
                </Modal>

                <Button transparent onPress={() => {
                    this.setModalVisible(true);
                }}>
                    <Icon name='add'/>
                </Button>

            </View>
        );
    }
}
const styles = StyleSheet.create({

    btnContainer: {
        flexDirection: "row",
        marginTop: 15,
    },

    btns: {
        width: '45%',
        marginLeft: '2.5%',
        marginRight: '2.5%',
        justifyContent: 'center',
    },

    textarea: {
        borderBottomWidth: 1,
        borderBottomColor: '#dedede',
        marginBottom: 10,
        paddingLeft: 5,
        paddingRight: 5
    },

});