import {createStackNavigator, createAppContainer} from 'react-navigation';
import Login from "./../page/Login"
import Template from "./../page/Templante"
import Inicio from "./../page/Inicio"
import Cuenta from "./../page/Cuenta"
import Invitados from "./../page/Invitados"
import Usuarios from "./../page/Usuarios"
import RegistroInvitado from "./../page/RegistroInvitado"
import RegistroUsuario from "./../page/RegistroUsuario"
import InfoInvitados from "./../page/InfoInvitados"
import InfoUsuarios from "./../page/InfoUsuarios"
import InfoNoticia from "./../page/InfoNoticia"

const MainNavigator = createStackNavigator({

    Login: {
        screen: Login,
        navigationOptions: {
            header: null,
        }
    },

    Usuarios: {
        screen: Usuarios,
        navigationOptions: {
            header: null,
        }
    },


    RegistroUsuario: {
        screen: RegistroUsuario,
        navigationOptions: {
            header: null,
        }
    },


    Invitados: {
        screen: Invitados,
        navigationOptions: {
            header: null,
        }
    },
    Inicio: {
        screen: Inicio,
        navigationOptions: {
            header: null,
        }
    },


    RegistroInvitado: {
        screen: RegistroInvitado,
        navigationOptions: {
            header: null,
        }
    },


    Cuenta: {
        screen: Cuenta,
        navigationOptions: {
            header: null,
        }
    },
    Template: {
        screen: Template,
        navigationOptions: {
            header: null,
        }
    },
    InfoInvitados: {
        screen: InfoInvitados,
        navigationOptions: {
            header: null,
        }
    },
    InfoUsuarios: {
        screen: InfoUsuarios,
        navigationOptions: {
            header: null,
        }
    },

    InfoNoticia: {
        screen: InfoNoticia,
        navigationOptions: {
            header: null,
        }
    },

});

const Navigation = createAppContainer(MainNavigator);

export default Navigation;