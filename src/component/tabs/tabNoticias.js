import React, {Component} from "react";
import {
    Content,
    Left,
    ListItem,
    Spinner,
    Text,
    Card,
    CardItem,
    Thumbnail,
    Image,
    Button,
    Body,
    Right,
    Icon
} from "native-base";

import {FlatList, StyleSheet} from "react-native";
import MethodsNoticias from "../../methods/MethodsNoticias";

export default class TabNoticias extends Component {

    constructor(props) {
        super(props);
        this.Noticias = new MethodsNoticias();
        this.state = {

            noticias: [],
            cargando: true
        };
    }

    componentDidMount() {

    }

    componentWillReceiveProps(nextProps) {
        //this.getInvitados().done();
        if (nextProps.noticias === this.state.noticias) {
            //no cambios
        } else {
            this.setState({noticias: nextProps.noticias});
        }
        //console.log(nextProps.cargando);
        this.setState({cargando: nextProps.cargando});

    }

    capitalize(s) {
        return s.toLowerCase().replace(/\b./g, function (a) {
            return a.toUpperCase();
        });
    };


    cargarNoticias(navigation) {
        //console.log(JSON.stringify(this.state.noticias))
        if (this.state.cargando)
            return (<Spinner color='blue'/>);
        else
            return (
                <FlatList
                    style={styles.listItem}
                    data={this.state.noticias}
                    extraData={this.state}
                    keyExtractor={(item, index) => String(index)}
                    renderItem={({item, index}) => {
                        return (
                            <ListItem
                                style={styles.cardContainer}
                                avatar
                                onPress={() => navigation.push('InfoNoticia', {noticia: item})}
                                selected={this.state.selected === item.titulo}
                            >
                                <Content>
                                    <Card>

                                        <CardItem style={styles.cardHeader}>
                                            <Left>
                                                <Thumbnail
                                                    source={{uri: "https://gospelapp.lasbienaventuranzas.org/images/perfil/" + item.imagen}}/>
                                                <Body>
                                                    <Text>{this.capitalize(item.titulo)}</Text>
                                                    <Text
                                                        note>{this.capitalize(item.autor.nombres + " " + item.autor.apellidos)}
                                                    </Text>
                                                    <Text note>{item.fecha}</Text>
                                                </Body>
                                            </Left>
                                        </CardItem>
                                        <CardItem style={styles.cardOptions}>

                                            <Left>
                                                <Button transparent>
                                                    {
                                                        item.like ? <Icon active name="md-heart"/> :
                                                            <Icon active name="md-heart-empty"/>
                                                    }
                                                    <Text>{item.likes}</Text>
                                                </Button>

                                            </Left>

                                            <Right>
                                                <Button transparent>
                                                    <Icon active name="eye"/>
                                                    <Text>{item.vistos}</Text>
                                                </Button>
                                            </Right>

                                        </CardItem>
                                    </Card>
                                </Content>
                            </ListItem>

                        );
                    }}
                />);
    }

    render() {
        const {navigation} = this.props;

        return (
            <Content padder>

                {this.cargarNoticias(navigation)}

            </Content>


        );
    }
}

const styles = StyleSheet.create({

    textCapitalize: {
        textTransform: 'capitalize',
    },

    listItem: {
        paddingStart: 0,
        //backgroundColor: '#ffe000'
    },

    cardContainer: {
        marginLeft: 0,
        paddingLeft: 0
    },

    iconCardNews: {
        color: '#495fdc',
        fontSize: 60,
    },

    cardHeader: {
        paddingBottom: 0,
    },

    cardOptions: {
        paddingTop: 0
    },

});
