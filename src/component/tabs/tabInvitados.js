import React, {Component} from "react";
import {
    Body,
    Content,
    Icon,
    Left,
    ListItem,
    Right,
    Spinner,
    Text,
    Thumbnail,
} from "native-base";
import {FlatList} from "react-native";

export default class TabInvitados extends Component {

    constructor(props) {
        super(props);
        this.state = {
            invitados: [],
            cargando: true
        };
    }

    componentDidMount() {
        //this.getInvitados().done();
        //console.log("Invitados:  tab montado");
        //console.log(this.state.invitados)
    }

    componentWillReceiveProps(nextProps) {
        // update original states

        //this.getInvitados().done();
        if (nextProps.invitados === this.state.invitados) {
            //no cambios
        } else {
            this.setState({invitados: nextProps.invitados});
        }
        //console.log(nextProps.cargando);
        this.setState({cargando: nextProps.cargando});


    }

    cargandoInvitados(navigation) {
        if (this.state.cargando)
            return (<Spinner color='blue'/>);
        else
            return (<FlatList
                    data={this.state.invitados}
                    extraData={this.state}
                    keyExtractor={(item, index) => String(index)}
                    renderItem={({item, index}) => {

                        return (
                            <ListItem
                                avatar
                                onPress={() => navigation.push('InfoInvitados', {invitado: item, cargo:'Pastor'})}
                                selected={this.state.selected === item.nombres}
                            >
                                <Left>
                                    <Thumbnail source={{ uri: "https://gospelapp.lasbienaventuranzas.org/images/perfil/avatar-1.png"  }} />
                                </Left>
                                <Body>
                                    <Text>{item.nombres} {item.apellidos}</Text>
                                    <Text note>{item.fecha_registro}</Text>
                                </Body>
                                <Right>
                                    <Icon name="arrow-forward"/>
                                </Right>
                            </ListItem>


                        );
                    }}
                />


            );
    }

    render() {
        const {navigation} = this.props;

        return (
            <Content padder>

                {this.cargandoInvitados(navigation)}

            </Content>


        );
    }
}
