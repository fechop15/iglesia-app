import React, {Component} from 'react';
import {Alert, AsyncStorage} from 'react-native';

export default class MethodsInvitados extends Component {


    async fetchInvitados(estado) {
        let  api_token = "";
        await AsyncStorage.getItem('user')
            .then((value) => {
                    api_token = JSON.parse(value).api_token;
                }
            );
        return  fetch('https://gospelapp.lasbienaventuranzas.org/api/v1/getInvitados', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                'api_token': api_token,
                'estado': estado
            }),
        }).then((response) => response.json())
            .then((responseJson) => {

               /* if (estado === '0') {
                    AsyncStorage.setItem('invitados', JSON.stringify(res.invitados));
                } else {
                    AsyncStorage.setItem('invitadosConfirmados', JSON.stringify(res.invitados));
                }*/
                return responseJson;

            }).catch((error) => {
                console.error(error);
                return error;
            });

    }

    async addInvitado(nombre,apellido, telefono, direccion, oracion) {
        let api_token = "";
        await AsyncStorage.getItem('user')
            .then((value) => {
                    api_token = JSON.parse(value).api_token;
                }
            );
        return fetch('https://gospelapp.lasbienaventuranzas.org/api/v1/addInvitado', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                api_token: api_token,
                nombres: nombre.toUpperCase() ,
                apellidos: apellido.toUpperCase(),
                telefono: telefono,
                direccion: direccion,
                oracion: oracion,

            })
        }).then((response) => response.json())
            .then((responseJson) => {

                //this.fetchInvitados('0');
                //this.fetchInvitados('1');

                return responseJson;

            }).catch((error) => {
                console.log("ERROR: "+error);
            });

    }

    async confirmarInvitado(idInvitado) {
        let api_token = "";
        await AsyncStorage.getItem('user')
            .then((value) => {
                    api_token = JSON.parse(value).api_token;
                }
            );
        return fetch('https://gospelapp.lasbienaventuranzas.org/api/v1/confirmarInvitado', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                api_token: api_token,
                id: idInvitado

            })
        }).then((response) => response.json())
            .then((responseJson) => {

                //this.fetchInvitados('0');
                //this.fetchInvitados('1');

                return responseJson;

            }).catch((error) => {
                console.log("ERROR: "+error);
            });

    }

    async eliminarInvitado(idInvitado) {
        let api_token = "";
        await AsyncStorage.getItem('user')
            .then((value) => {
                    api_token = JSON.parse(value).api_token;
                }
            );
        return fetch('https://gospelapp.lasbienaventuranzas.org/api/v1/eliminarInvitado', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                api_token: api_token,
                id: idInvitado

            })
        }).then((response) => response.json())
            .then((responseJson) => {

                //this.fetchInvitados('0');
                //this.fetchInvitados('1');

                return responseJson;

            }).catch((error) => {
                console.error(error);
            });

    }


}