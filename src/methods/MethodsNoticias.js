import React, {Component} from 'react';
import {Alert, AsyncStorage} from 'react-native';


const dias = ["Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo"];
const meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];

export default class MethodsNoticias extends Component {


    obtenerFecha(fechaCreacion) {

        let date = new Date(fechaCreacion);
        let fechaNum = date.getDate() + 1;
        let mes_name = date.getMonth();
        return (dias[date.getDay()] + " " + fechaNum + " de " + meses[mes_name] + " de " + date.getFullYear());

    }

    async fetchNoticias(tipo) {
        let api_token = "";
        await AsyncStorage.getItem('user')
            .then((value) => {
                    api_token = JSON.parse(value).api_token;
                }
            );
        return fetch('https://gospelapp.lasbienaventuranzas.org/api/v1/getNoticias', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                'api_token': api_token,
                'tipo': tipo
            }),
        }).then((response) => response.json())
            .then((responseJson) => {

                console.log(responseJson)

                /* if (estado === '0') {
                     AsyncStorage.setItem('invitados', JSON.stringify(res.invitados));
                 } else {
                     AsyncStorage.setItem('invitadosConfirmados', JSON.stringify(res.invitados));
                 }*/
                return responseJson;

            }).catch((error) => {
                console.error(error);
                return error;
            });

    }

    async visitaNoticia(idNoticia, like) {

        let api_token = "";
        await AsyncStorage.getItem('user')
            .then((value) => {
                    api_token = JSON.parse(value).api_token;
                }
            );
        return fetch('https://gospelapp.lasbienaventuranzas.org/api/v1/visitaNoticia', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                'api_token': api_token,
                'idNoticia': idNoticia,
                'like': like,
            }),
        }).then((response) => response.json())
            .then((responseJson) => {

                console.log(responseJson)

                /* if (estado === '0') {
                     AsyncStorage.setItem('invitados', JSON.stringify(res.invitados));
                 } else {
                     AsyncStorage.setItem('invitadosConfirmados', JSON.stringify(res.invitados));
                 }*/
                return responseJson;

            }).catch((error) => {
                console.error(error);
                return error;
            });

    }

    async likeNoticia(idNoticia, like) {

        let api_token = "";
        await AsyncStorage.getItem('user')
            .then((value) => {
                    api_token = JSON.parse(value).api_token;
                }
            );
        return fetch('https://gospelapp.lasbienaventuranzas.org/api/v1/like', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                'api_token': api_token,
                'idNoticia': idNoticia,
                'like': like,
            }),
        }).then((response) => response.json())
            .then((responseJson) => {

                console.log(responseJson)

                /* if (estado === '0') {
                     AsyncStorage.setItem('invitados', JSON.stringify(res.invitados));
                 } else {
                     AsyncStorage.setItem('invitadosConfirmados', JSON.stringify(res.invitados));
                 }*/
                return responseJson;

            }).catch((error) => {
                console.error(error);
                return error;
            });

    }

    async getUltimaNoticia() {


        let api_token = "";
        await AsyncStorage.getItem('user')
            .then((value) => {
                    api_token = JSON.parse(value).api_token;
                }
            );
        return fetch('https://gospelapp.lasbienaventuranzas.org/api/v1/getUltimaNoticia', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                'api_token': api_token,
            }),
        }).then((response) => response.json())
            .then((responseJson) => {

                console.log(responseJson)

                /* if (estado === '0') {
                     AsyncStorage.setItem('invitados', JSON.stringify(res.invitados));
                 } else {
                     AsyncStorage.setItem('invitadosConfirmados', JSON.stringify(res.invitados));
                 }*/
                return responseJson;

            }).catch((error) => {
                console.error(error);
                return error;
            });

    }

    /*
    async addInvitado(nombre, apellido, telefono, direccion, oracion) {
        let api_token = "";
        await AsyncStorage.getItem('user')
            .then((value) => {
                    api_token = JSON.parse(value).api_token;
                }
            );
        return fetch('https://gospelapp.lasbienaventuranzas.org/api/v1/addInvitado', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                api_token: api_token,
                nombres: nombre.toUpperCase(),
                apellidos: apellido.toUpperCase(),
                telefono: telefono,
                direccion: direccion,
                oracion: oracion,

            })
        }).then((response) => response.json())
            .then((responseJson) => {

                //this.fetchInvitados('0');
                //this.fetchInvitados('1');

                return responseJson;

            }).catch((error) => {
                console.log("ERROR: " + error);
            });

    }

    async confirmarInvitado(idInvitado) {
        let api_token = "";
        await AsyncStorage.getItem('user')
            .then((value) => {
                    api_token = JSON.parse(value).api_token;
                }
            );
        return fetch('https://gospelapp.lasbienaventuranzas.org/api/v1/confirmarInvitado', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                api_token: api_token,
                id: idInvitado

            })
        }).then((response) => response.json())
            .then((responseJson) => {

                //this.fetchInvitados('0');
                //this.fetchInvitados('1');

                return responseJson;

            }).catch((error) => {
                console.log("ERROR: " + error);
            });

    }

    async eliminarInvitado(idInvitado) {
        let api_token = "";
        await AsyncStorage.getItem('user')
            .then((value) => {
                    api_token = JSON.parse(value).api_token;
                }
            );
        return fetch('https://gospelapp.lasbienaventuranzas.org/api/v1/eliminarInvitado', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                api_token: api_token,
                id: idInvitado

            })
        }).then((response) => response.json())
            .then((responseJson) => {

                //this.fetchInvitados('0');
                //this.fetchInvitados('1');

                return responseJson;

            }).catch((error) => {
                console.error(error);
            });

    }
    */

}