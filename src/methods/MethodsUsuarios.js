import React, {Component} from 'react';
import {Alert, AsyncStorage} from 'react-native';

export default class MethodsUsuarios extends Component {


    async fetchUsuario() {
        let api_token = "";
        await AsyncStorage.getItem('user')
            .then((value) => {
                api_token = JSON.parse(value).api_token;
                }
            );
        await fetch('https://gospelapp.lasbienaventuranzas.org/api/v1/getUsuarios', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                'api_token': api_token,
            }),
        }).then((response) => response.json())
            .then((res) => {

                AsyncStorage.setItem('usuarios', JSON.stringify(res.usuarios));
                return res.usuarios;
            }).done();

    }

    async addUsuario(nombre, correo, fecha, rol, telefono, password, direccion) {
        let api_token = "";
        await AsyncStorage.getItem('user')
            .then((value) => {
                    api_token = JSON.parse(value).api_token;
                }
            );
        return fetch('http://www.lasbienaventuranzas.org/api/addUsuario', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                api_token: api_token,
                nombre: nombre,
                correo: correo,
                fecha: fecha,
                rol: rol,
                telefono: telefono,
                password: password,
                direccion: direccion,

            })
        }).then((response) => response.json())
            .then((responseJson) => {

                this.fetchUsuario();

                return responseJson;

            }).catch((error) => {
                console.error(error);
            });

    }

    async confirmarInvitado(idInvitado) {
        let api_token = "";
        await AsyncStorage.getItem('user')
            .then((value) => {
                api_token = JSON.parse(value).api_token;
                }
            );
        return fetch('http://www.lasbienaventuranzas.org/api/confirmarInvitado', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                api_token: api_token,
                id: idInvitado

            })
        }).then((response) => response.json())
            .then((responseJson) => {

                this.fetchInvitados('0');
                this.fetchInvitados('1');

                return responseJson;

            }).catch((error) => {
                console.error(error);
            });

    }

    async eliminarInvitado(idInvitado) {
        let api_token = "";
        await AsyncStorage.getItem('user')
            .then((value) => {
                api_token = JSON.parse(value).api_token;
                }
            );
        return fetch('http://www.lasbienaventuranzas.org/api/eliminarInvitado', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                api_token: api_token,
                id: idInvitado

            })
        }).then((response) => response.json())
            .then((responseJson) => {

                this.fetchInvitados('0');
                this.fetchInvitados('1');

                return responseJson;

            }).catch((error) => {
                console.error(error);
            });

    }


}